CREATE TABLE IF NOT EXISTS coches(
  id INT(4) NOT null,
  marca varchar(50),
  fecha date,
  precio float,
  PRIMARY KEY (id)
  )
  CHARACTER SET utf8 COLLATE utf8_spanish_ci;

CREATE TABLE IF NOT EXISTS clientes(
  cod INT(4) NOT null,
  nombre varchar(50),
  IdCocheAlquilado int(4) NOT NULL UNIQUE,
  fechaAlquiler date,
  PRIMARY KEY (cod)
  )
  CHARACTER SET utf8 COLLATE utf8_spanish_ci;

ALTER TABLE clientes
  ADD CONSTRAINT FK_clientes_coches_idCocheAlquilado FOREIGN KEY (IdCocheAlquilado) 
    REFERENCES coches(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

