<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property int $IdCocheAlquilado
 * @property string|null $fechaAlquiler
 *
 * @property Coches $idCocheAlquilado
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod', 'IdCocheAlquilado'], 'required'],
            [['cod', 'IdCocheAlquilado'], 'integer'],
            [['fechaAlquiler'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['IdCocheAlquilado'], 'unique'],
            [['cod'], 'unique'],
            [['IdCocheAlquilado'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['IdCocheAlquilado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'IdCocheAlquilado' => 'Id Coche Alquilado',
            'fechaAlquiler' => 'Fecha Alquiler',
        ];
    }

    /**
     * Gets query for [[IdCocheAlquilado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCocheAlquilado()
    {
        return $this->hasOne(Coches::className(), ['id' => 'IdCocheAlquilado']);
    }
}
